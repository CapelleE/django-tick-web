# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ticketing', '0023_auto_20150420_1839'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClassePourExemple',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('champsUn', models.CharField(blank=True, max_length=10, verbose_name='Champs un', null=True)),
                ('champsDeux', models.BooleanField(verbose_name='Champs booléen')),
                ('fk_referenceIdentite', models.ForeignKey(to='ticketing.ClassePourExemple', blank=True, null=True, default=None, verbose_name='Champs self-référence')),
            ],
        ),
    ]
